<?php
App::uses('AppController', 'Controller');
/**
 * Russianwords Controller
 *
 * @property Russianword $Russianword
 */
class RussianwordsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Russianword->recursive = 0;
		$this->set('russianwords', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Russianword->exists($id)) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		$options = array('conditions' => array('Russianword.' . $this->Russianword->primaryKey => $id));
		$this->set('russianword', $this->Russianword->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Russianword->create();
			if ($this->Russianword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianword could not be saved. Please, try again.'));
			}
		}
		$spanishwords = $this->Russianword->Spanishword->find('list');
		$this->set(compact('spanishwords'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Russianword->exists($id)) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Russianword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Russianword.' . $this->Russianword->primaryKey => $id));
			$this->request->data = $this->Russianword->find('first', $options);
		}
		$spanishwords = $this->Russianword->Spanishword->find('list');
		$this->set(compact('spanishwords'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Russianword->id = $id;
		if (!$this->Russianword->exists()) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Russianword->delete()) {
			$this->Session->setFlash(__('Russianword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Russianword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Russianword->recursive = 0;
		$this->set('russianwords', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Russianword->exists($id)) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		$options = array('conditions' => array('Russianword.' . $this->Russianword->primaryKey => $id));
		$this->set('russianword', $this->Russianword->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Russianword->create();
			if ($this->Russianword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianword could not be saved. Please, try again.'));
			}
		}
		$spanishwords = $this->Russianword->Spanishword->find('list');
		$this->set(compact('spanishwords'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Russianword->exists($id)) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Russianword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Russianword.' . $this->Russianword->primaryKey => $id));
			$this->request->data = $this->Russianword->find('first', $options);
		}
		$spanishwords = $this->Russianword->Spanishword->find('list');
		$this->set(compact('spanishwords'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Russianword->id = $id;
		if (!$this->Russianword->exists()) {
			throw new NotFoundException(__('Invalid russianword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Russianword->delete()) {
			$this->Session->setFlash(__('Russianword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Russianword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
