<?php
App::uses('AppController', 'Controller');
/**
 * RussianwordsSpanishwords Controller
 *
 * @property RussianwordsSpanishword $RussianwordsSpanishword
 */
class RussianwordsSpanishwordsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RussianwordsSpanishword->recursive = 0;
		$this->set('russianwordsSpanishwords', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RussianwordsSpanishword->exists($id)) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		$options = array('conditions' => array('RussianwordsSpanishword.' . $this->RussianwordsSpanishword->primaryKey => $id));
		$this->set('russianwordsSpanishword', $this->RussianwordsSpanishword->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RussianwordsSpanishword->create();
			if ($this->RussianwordsSpanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianwords spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianwords spanishword could not be saved. Please, try again.'));
			}
		}
		$russianwords = $this->RussianwordsSpanishword->Russianword->find('list');
		$spanishwords = $this->RussianwordsSpanishword->Spanishword->find('list');
		$this->set(compact('russianwords', 'spanishwords'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RussianwordsSpanishword->exists($id)) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RussianwordsSpanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianwords spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianwords spanishword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RussianwordsSpanishword.' . $this->RussianwordsSpanishword->primaryKey => $id));
			$this->request->data = $this->RussianwordsSpanishword->find('first', $options);
		}
		$russianwords = $this->RussianwordsSpanishword->Russianword->find('list');
		$spanishwords = $this->RussianwordsSpanishword->Spanishword->find('list');
		$this->set(compact('russianwords', 'spanishwords'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RussianwordsSpanishword->id = $id;
		if (!$this->RussianwordsSpanishword->exists()) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RussianwordsSpanishword->delete()) {
			$this->Session->setFlash(__('Russianwords spanishword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Russianwords spanishword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RussianwordsSpanishword->recursive = 0;
		$this->set('russianwordsSpanishwords', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RussianwordsSpanishword->exists($id)) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		$options = array('conditions' => array('RussianwordsSpanishword.' . $this->RussianwordsSpanishword->primaryKey => $id));
		$this->set('russianwordsSpanishword', $this->RussianwordsSpanishword->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RussianwordsSpanishword->create();
			if ($this->RussianwordsSpanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianwords spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianwords spanishword could not be saved. Please, try again.'));
			}
		}
		$russianwords = $this->RussianwordsSpanishword->Russianword->find('list');
		$spanishwords = $this->RussianwordsSpanishword->Spanishword->find('list');
		$this->set(compact('russianwords', 'spanishwords'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RussianwordsSpanishword->exists($id)) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RussianwordsSpanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The russianwords spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The russianwords spanishword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RussianwordsSpanishword.' . $this->RussianwordsSpanishword->primaryKey => $id));
			$this->request->data = $this->RussianwordsSpanishword->find('first', $options);
		}
		$russianwords = $this->RussianwordsSpanishword->Russianword->find('list');
		$spanishwords = $this->RussianwordsSpanishword->Spanishword->find('list');
		$this->set(compact('russianwords', 'spanishwords'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RussianwordsSpanishword->id = $id;
		if (!$this->RussianwordsSpanishword->exists()) {
			throw new NotFoundException(__('Invalid russianwords spanishword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RussianwordsSpanishword->delete()) {
			$this->Session->setFlash(__('Russianwords spanishword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Russianwords spanishword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
