<?php
App::uses('AppController', 'Controller');
/**
 * Spanishwords Controller
 *
 * @property Spanishword $Spanishword
 */
class SpanishwordsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Spanishword->recursive = 1;
		$this->set('spanishwords', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Spanishword->exists($id)) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		$options = array('conditions' => array('Spanishword.' . $this->Spanishword->primaryKey => $id));
		$this->set('spanishword', $this->Spanishword->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Spanishword->create();
			if ($this->Spanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The spanishword could not be saved. Please, try again.'));
			}
		}
		$russianwords = $this->Spanishword->Russianword->find('list');
		$this->set(compact('russianwords'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Spanishword->exists($id)) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Spanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The spanishword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Spanishword.' . $this->Spanishword->primaryKey => $id));
			$this->request->data = $this->Spanishword->find('first', $options);
		}
		$russianwords = $this->Spanishword->Russianword->find('list');
		$this->set(compact('russianwords'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Spanishword->id = $id;
		if (!$this->Spanishword->exists()) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Spanishword->delete()) {
			$this->Session->setFlash(__('Spanishword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Spanishword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Spanishword->recursive = 0;
		$this->set('spanishwords', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Spanishword->exists($id)) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		$options = array('conditions' => array('Spanishword.' . $this->Spanishword->primaryKey => $id));
		$this->set('spanishword', $this->Spanishword->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Spanishword->create();
			if ($this->Spanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The spanishword could not be saved. Please, try again.'));
			}
		}
		$russianwords = $this->Spanishword->Russianword->find('list');
		$this->set(compact('russianwords'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Spanishword->exists($id)) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Spanishword->save($this->request->data)) {
				$this->Session->setFlash(__('The spanishword has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The spanishword could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Spanishword.' . $this->Spanishword->primaryKey => $id));
			$this->request->data = $this->Spanishword->find('first', $options);
		}
		$russianwords = $this->Spanishword->Russianword->find('list');
		$this->set(compact('russianwords'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Spanishword->id = $id;
		if (!$this->Spanishword->exists()) {
			throw new NotFoundException(__('Invalid spanishword'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Spanishword->delete()) {
			$this->Session->setFlash(__('Spanishword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Spanishword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
