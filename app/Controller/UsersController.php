<?php

class UsersController extends AppController {
    var $name = 'Users';
    var $helpers = array(
        'Html',
        'Form'
    );
    
    public function add() {
        if(!empty($this->data)) {
            $this->User->create();
            if ($this->User->save($this->data)) {
                $this->Session->setFlash('Usuario creado exitosamente');
                $this->redirect('/');
            } else {
                $this->Session->setFlash('Error creado en usuario');
            }
        }
    }
}
?>
