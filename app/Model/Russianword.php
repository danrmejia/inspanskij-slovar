<?php
App::uses('AppModel', 'Model');
/**
 * Russianword Model
 *
 * @property Spanishword $Spanishword
 */
class Russianword extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'word';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'word' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Spanishword' => array(
			'className' => 'Spanishword',
			'joinTable' => 'russianwords_spanishwords',
			'foreignKey' => 'russianword_id',
			'associationForeignKey' => 'spanishword_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
