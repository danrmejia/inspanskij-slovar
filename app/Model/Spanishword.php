<?php
App::uses('AppModel', 'Model');
/**
 * Spanishword Model
 *
 * @property Russianword $Russianword
 */
class Spanishword extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'word';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'word' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Russianword' => array(
			'className' => 'Russianword',
			'joinTable' => 'russianwords_spanishwords',
			'foreignKey' => 'spanishword_id',
			'associationForeignKey' => 'russianword_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
