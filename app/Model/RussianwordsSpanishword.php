<?php
App::uses('AppModel', 'Model');
/**
 * RussianwordsSpanishword Model
 *
 * @property Russianword $Russianword
 * @property Spanishword $Spanishword
 */
class RussianwordsSpanishword extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'russianword_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'spanishword_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Russianword' => array(
			'className' => 'Russianword',
			'foreignKey' => 'russianword_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Spanishword' => array(
			'className' => 'Spanishword',
			'foreignKey' => 'spanishword_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
