CREATE TABLE `users`  (
    ` id`  int(10) unsigned NOT NULL auto_increment,
    ` first_name`  varchar(45) NOT NULL,
    ` last_name`  varchar(45) NOT NULL,
    ` email`  varchar(150) NOT NULL,
    ` password`  varchar(45) NOT NULL,
    ` address`  varchar(45) default NULL,
    ` address2`  varchar(45) default NULL,
    ` city`  varchar(45) default NULL,
    ` state`  varchar(45) default NULL,
    ` zipcode`  varchar(10) default NULL,
    ` country`  varchar(45) default NULL,
    ` created`  datetime default NULL,
    ` modified`  datetime default NULL,
    PRIMARY KEY (` id` )
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
