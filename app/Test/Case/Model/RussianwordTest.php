<?php
App::uses('Russianword', 'Model');

/**
 * Russianword Test Case
 *
 */
class RussianwordTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.russianword',
		'app.spanishword',
		'app.russianwords_spanishword'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Russianword = ClassRegistry::init('Russianword');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Russianword);

		parent::tearDown();
	}

}
