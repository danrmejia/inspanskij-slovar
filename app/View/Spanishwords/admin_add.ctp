<div class="spanishwords form">
<?php echo $this->Form->create('Spanishword'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Spanishword'); ?></legend>
	<?php
		echo $this->Form->input('word');
		echo $this->Form->input('definition');
		echo $this->Form->input('phonetics');
		echo $this->Form->input('synonym');
		echo $this->Form->input('Russianword');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Spanishwords'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('controller' => 'russianwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
