<div class="spanishwords view">
<h2><?php  echo __('Spanishword'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($spanishword['Spanishword']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Word'); ?></dt>
		<dd>
			<?php echo h($spanishword['Spanishword']['word']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Definition'); ?></dt>
		<dd>
			<?php echo h($spanishword['Spanishword']['definition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phonetics'); ?></dt>
		<dd>
			<?php echo h($spanishword['Spanishword']['phonetics']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Synonym'); ?></dt>
		<dd>
			<?php echo h($spanishword['Spanishword']['synonym']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Spanishword'), array('action' => 'edit', $spanishword['Spanishword']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Spanishword'), array('action' => 'delete', $spanishword['Spanishword']['id']), null, __('Are you sure you want to delete # %s?', $spanishword['Spanishword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('controller' => 'russianwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Russianwords'); ?></h3>
	<?php if (!empty($spanishword['Russianword'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Word'); ?></th>
		<th><?php echo __('Definition'); ?></th>
		<th><?php echo __('Phonetics'); ?></th>
		<th><?php echo __('Synonym'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($spanishword['Russianword'] as $russianword): ?>
		<tr>
			<td><?php echo $russianword['id']; ?></td>
			<td><?php echo $russianword['word']; ?></td>
			<td><?php echo $russianword['definition']; ?></td>
			<td><?php echo $russianword['phonetics']; ?></td>
			<td><?php echo $russianword['synonym']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'russianwords', 'action' => 'view', $russianword['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'russianwords', 'action' => 'edit', $russianword['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'russianwords', 'action' => 'delete', $russianword['id']), null, __('Are you sure you want to delete # %s?', $russianword['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
