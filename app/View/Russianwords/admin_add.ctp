<div class="russianwords form">
<?php echo $this->Form->create('Russianword'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Russianword'); ?></legend>
	<?php
		echo $this->Form->input('word');
		echo $this->Form->input('definition');
		echo $this->Form->input('phonetics');
		echo $this->Form->input('synonym');
		echo $this->Form->input('Spanishword');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Russianwords'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('controller' => 'spanishwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
