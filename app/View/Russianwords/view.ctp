<div class="russianwords view">
<h2><?php  echo __('Russianword'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($russianword['Russianword']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Word'); ?></dt>
		<dd>
			<?php echo h($russianword['Russianword']['word']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Definition'); ?></dt>
		<dd>
			<?php echo h($russianword['Russianword']['definition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phonetics'); ?></dt>
		<dd>
			<?php echo h($russianword['Russianword']['phonetics']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Synonym'); ?></dt>
		<dd>
			<?php echo h($russianword['Russianword']['synonym']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Russianword'), array('action' => 'edit', $russianword['Russianword']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Russianword'), array('action' => 'delete', $russianword['Russianword']['id']), null, __('Are you sure you want to delete # %s?', $russianword['Russianword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('controller' => 'spanishwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Spanishwords'); ?></h3>
	<?php if (!empty($russianword['Spanishword'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Word'); ?></th>
		<th><?php echo __('Definition'); ?></th>
		<th><?php echo __('Phonetics'); ?></th>
		<th><?php echo __('Synonym'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($russianword['Spanishword'] as $spanishword): ?>
		<tr>
			<td><?php echo $spanishword['id']; ?></td>
			<td><?php echo $spanishword['word']; ?></td>
			<td><?php echo $spanishword['definition']; ?></td>
			<td><?php echo $spanishword['phonetics']; ?></td>
			<td><?php echo $spanishword['synonym']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'spanishwords', 'action' => 'view', $spanishword['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'spanishwords', 'action' => 'edit', $spanishword['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'spanishwords', 'action' => 'delete', $spanishword['id']), null, __('Are you sure you want to delete # %s?', $spanishword['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
