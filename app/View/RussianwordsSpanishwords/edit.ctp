<div class="russianwordsSpanishwords form">
<?php echo $this->Form->create('RussianwordsSpanishword'); ?>
	<fieldset>
		<legend><?php echo __('Edit Russianwords Spanishword'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('russianword_id');
		echo $this->Form->input('spanishword_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RussianwordsSpanishword.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('RussianwordsSpanishword.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Russianwords Spanishwords'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('controller' => 'russianwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('controller' => 'spanishwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
