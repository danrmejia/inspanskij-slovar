<div class="russianwordsSpanishwords index">
	<h2><?php echo __('Russianwords Spanishwords'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('russianword_id'); ?></th>
			<th><?php echo $this->Paginator->sort('spanishword_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($russianwordsSpanishwords as $russianwordsSpanishword): ?>
	<tr>
		<td><?php echo h($russianwordsSpanishword['RussianwordsSpanishword']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($russianwordsSpanishword['Russianword']['word'], array('controller' => 'russianwords', 'action' => 'view', $russianwordsSpanishword['Russianword']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($russianwordsSpanishword['Spanishword']['word'], array('controller' => 'spanishwords', 'action' => 'view', $russianwordsSpanishword['Spanishword']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $russianwordsSpanishword['RussianwordsSpanishword']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $russianwordsSpanishword['RussianwordsSpanishword']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $russianwordsSpanishword['RussianwordsSpanishword']['id']), null, __('Are you sure you want to delete # %s?', $russianwordsSpanishword['RussianwordsSpanishword']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Russianwords Spanishword'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('controller' => 'russianwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('controller' => 'spanishwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
