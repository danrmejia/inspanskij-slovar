<div class="russianwordsSpanishwords view">
<h2><?php  echo __('Russianwords Spanishword'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($russianwordsSpanishword['RussianwordsSpanishword']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Russianword'); ?></dt>
		<dd>
			<?php echo $this->Html->link($russianwordsSpanishword['Russianword']['word'], array('controller' => 'russianwords', 'action' => 'view', $russianwordsSpanishword['Russianword']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Spanishword'); ?></dt>
		<dd>
			<?php echo $this->Html->link($russianwordsSpanishword['Spanishword']['word'], array('controller' => 'spanishwords', 'action' => 'view', $russianwordsSpanishword['Spanishword']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Russianwords Spanishword'), array('action' => 'edit', $russianwordsSpanishword['RussianwordsSpanishword']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Russianwords Spanishword'), array('action' => 'delete', $russianwordsSpanishword['RussianwordsSpanishword']['id']), null, __('Are you sure you want to delete # %s?', $russianwordsSpanishword['RussianwordsSpanishword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Russianwords Spanishwords'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianwords Spanishword'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Russianwords'), array('controller' => 'russianwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Russianword'), array('controller' => 'russianwords', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Spanishwords'), array('controller' => 'spanishwords', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Spanishword'), array('controller' => 'spanishwords', 'action' => 'add')); ?> </li>
	</ul>
</div>
